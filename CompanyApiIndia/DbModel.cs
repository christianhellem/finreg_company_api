﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyApiIndia
{
    public class DbModel
    {
        public class IndianContext : DbContext
        {
            public IndianContext() : base("name=IndianCompanies")
            {
                Database.SetInitializer(new MigrateDatabaseToLatestVersion<IndianContext, Migrations.Configuration>());

            }  

            public DbSet<IndianCompany> IndianCompanies { get; set; }
        }

        public class IndianCompany
        {
            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public Guid Key{ get; set; }

            public DateTime Imported { get; set; }
            
            public string RegionIdentifier { get; set; }
            public string corporate_identification_number { get; set; }
            public string date_of_registration { get; set; }
            public string company_name { get; set; }
            public string company_status { get; set; }
            public string company_class { get; set; }
            public string company_category { get; set; }
            public string authorized_capital { get; set; }
            public string paidup_capital { get; set; }
            public string registered_state { get; set; }
            public string registrar_of_companies { get; set; }
            public string principal_business_activity { get; set; }
            public string registered_office_address { get; set; }
            public string sub_category { get; set; }
        }
    }
}