USE [IndianCompanies]
GO

/****** Object:  Table [dbo].[IndianCompanies]    Script Date: 08-04-2018 21:46:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IndianCompanies](
	[Key] [uniqueidentifier] NOT NULL,
	[RegionIdentifier] [nvarchar](max) NULL,
	[_corporate_identification_number_] [nvarchar](max) NULL,
	[_date_of_registration_] [nvarchar](max) NULL,
	[_company_name_] [nvarchar](max) NULL,
	[_company_status_] [nvarchar](max) NULL,
	[_company_class_] [nvarchar](max) NULL,
	[_company_category_] [nvarchar](max) NULL,
	[_authorized_capital_] [nvarchar](max) NULL,
	[_paidup_capital_] [nvarchar](max) NULL,
	[_registered_state_] [nvarchar](max) NULL,
	[_registrar_of_companies_] [nvarchar](max) NULL,
	[_principal_business_activity_] [nvarchar](max) NULL,
	[_registered_office_address_] [nvarchar](max) NULL,
	[_sub_category_] [nvarchar](max) NULL,
	[Imported] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.IndianCompanies] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[IndianCompanies] ADD  DEFAULT (newsequentialid()) FOR [Key]
GO

ALTER TABLE [dbo].[IndianCompanies] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [Imported]
GO


