﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;
using static CompanyApiIndia.DbModel;

namespace CompanyApiIndia
{
    public class FetchUrl
    {

        private string apiKey = "579b464db66ec23bdd000001df862bf7324f4e78636e51e0daf14292";
        private string uriParams = "&format=json";
        private int limit = int.Parse(ConfigurationManager.AppSettings["fetchlimit"]);
        private int pagesize = int.Parse(ConfigurationManager.AppSettings["pagesize"]);
        

        public int Limit { get { return limit; } }
        public int Offset { get; set; }
        public int PageSize { get { return pagesize; } }

        public string Name { get; set; }
        public string Ressource { get; set; }
        public string Url { get { return "https://api.data.gov.in" + this.Ressource + 
                    "?api-key=" + apiKey + 
                    uriParams +
                    "&offset="+ Offset + 
                    "&limit=" + PageSize;
            }
        }

        public void NextPage()
        {
            Offset += PageSize;
        }

        public FetchUrl()
        {
            Offset = 0;
        }
    }

    public class IndianApiConnection
    {

        private List<FetchUrl> fetchUrls = new List<FetchUrl>();

        public IndianApiConnection()
        {
            fetchUrls.Add(new FetchUrl()
            { Name = "Telangana", Ressource = "/resource/071758ef-8b2b-4ff6-8774-bcf782214779" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Bihar", Ressource = "/resource/3f328009-8f64-426d-9228-750a3fe8e326" });
            fetchUrls.Add(new FetchUrl()
            { Name = "West Bengal", Ressource = "/resource/ccd42a4e-b657-4244-a43f-a203e3cf7dd8" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Uttar Pradesh", Ressource = "/resource/f8547c08-a7bf-4e85-b179-c57b5bd135a8" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Uttarakhand", Ressource = "/resource/74a2d302-e24f-42cf-b95c-ff279bcf133d" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Tripura", Ressource = "/resource/a1513fa4-007e-4085-a367-7a65562e9bf4" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Odisha", Ressource = "/resource/997ad190-4308-4d8e-808c-8148c2c9ed08" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Puducherry", Ressource = "/resource/f4a928ea-757e-462c-957e-f783f6cfc206" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Punjab", Ressource = "/resource/3bac7cea-66b4-49b0-b310-4cd730e28287" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Rajasthan", Ressource = "/resource/133dd8f2-44b3-4a6d-a208-72b1030c51fb" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Tamil Nadu", Ressource = "/resource/73d8110b-4492-48b5-9f8b-b5bf2ce65261" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Maharashtra", Ressource = "/resource/d1ac29db-549d-44b2-9bea-28d6e449ff85" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Manipur", Ressource = "/resource/44486d32-3c20-41f4-9376-9f4ac360eaa1" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Meghalaya", Ressource = "/resource/57ae016f-b67f-42dc-b473-2fdae3621f3b" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Mizoram", Ressource = "/resource/76fdab68-795b-42f4-bc6d-188442b3ff57" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Nagaland", Ressource = "/resource/6a6e802c-66e2-47c2-ad20-4abc9289c85b" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Madhya Pradesh", Ressource = "/resource/f526be27-c0bf-4d99-b931-0f8e247e59d0" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Gujarat", Ressource = "/resource/1ea03789-3147-4a39-a85e-22f4ca128689" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Haryana", Ressource = "/resource/fc0730f1-9736-409d-b3d8-0ac64122c225" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Himachal Pradesh", Ressource = "/resource/da1e82e7-fb09-48b3-96cb-8fd0411d4ee6" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Jammu and Kashmir", Ressource = "/resource/f8dd5590-8843-49be-9ae2-79c5b3e23ed0" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Jharkhand", Ressource = "/resource/b4eb9d9b-c8e7-4ec3-b564-e6a018f7249e" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Karnataka", Ressource = "/resource/080e668f-1e57-4376-8269-b41ca9c39cc6" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Kerala", Ressource = "/resource/071aa695-4a6e-4bb9-a109-6e9da1329967" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Lakshadweep", Ressource = "/resource/37cb05be-4210-432d-a19a-423ebfe374dd" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Chhattisgarh", Ressource = "/resource/4081f64b-6702-46de-b380-d73edf1ca395" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Daman and Diu", Ressource = "/resource/6a0b8194-3e00-4a2b-908e-04470a1f98b3" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Delhi", Ressource = "/resource/7502bd54-2f04-43a5-ae40-437628b0145a" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Goa", Ressource = "/resource/4dbe5667-7b6b-41d7-82af-211562424d9a" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Assam", Ressource = "/resource/6a48e198-1b5c-46e6-ad9e-789b231992c1" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Arunachal Pradesh", Ressource = "/resource/8173b4fa-001a-4891-9806-057d87a60fe8" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Andhra Pradesh", Ressource = "/resource/006e6aff-6108-4bb6-ba60-ecd9b83a5280" });
            fetchUrls.Add(new FetchUrl()
            { Name = "A&N Islands", Ressource = "/resource/6f1d971f-ea19-4bbe-b956-2568887c1f37" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Chandigarh", Ressource = "/resource/df73b4ed-2355-4f2e-9392-4b3201bde8b3" });
            fetchUrls.Add(new FetchUrl()
            { Name = "Dadra & Nagar Haveli", Ressource = "/resource/fe6081c0-a880-44b4-9acd-715d73b4032f" });
        }

        //api key 579b464db66ec23bdd000001df862bf7324f4e78636e51e0daf14292
        //https://api.data.gov.in/resource/3f328009-8f64-426d-9228-750a3fe8e326?api-key=579b464db66ec23bdd000001df862bf7324f4e78636e51e0daf14292&format=json&offset=0&limit=10

        public void GetData()
        {
            foreach (var region in fetchUrls)
                GetData(region);
        }

        public void GetData(FetchUrl url)
        {
            Console.WriteLine("Getting data from " + url.Name);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            int count = 0;
            string result;

            while (true)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url.Url);

                if (count + url.PageSize > url.Limit)
                {
                    Console.WriteLine("Reached Limit of maximum " + url.Limit);
                    Console.WriteLine("Saved " + count + " companies to the database...");
                    break;
                }

                Console.WriteLine("Fetching companies number " + url.Offset + " to " + (url.Offset + url.PageSize).ToString() + "...");

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        result = reader.ReadToEnd();
                    }
                }

                var iterationCount = PushJsonStringToDatabase(result, url);
                count += iterationCount;

                if (iterationCount < url.PageSize)
                    break;

                url.NextPage();
            }

            Console.WriteLine("Found a total of " + count.ToString() + " companies for " + url.Name);
        }

        public void ClearCompanies()
        {
            Console.WriteLine("Clearing import tables...");

            using (var context = new IndianContext())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [IndianCompanies]");
            }
        }

        private int PushJsonStringToDatabase(string jsonData, FetchUrl url)
        {
            JObject jo = JObject.Parse(jsonData);
            JToken Records = jo.Descendants()
                .Where(x => x.Type == JTokenType.Property && ((JProperty)x).Name == "records").FirstOrDefault();

            var companies = JsonConvert.DeserializeObject<List<DbModel.IndianCompany>>(Records.FirstOrDefault().ToString())
                .Select(c => { c.Imported = DateTime.Now; return c; })
                .Select(c => { c.RegionIdentifier = url.Name; return c; });


            using (var context = new IndianContext())
            {
                context.IndianCompanies.AddRange(companies);
                context.SaveChanges();
            }

            return companies.Count();
        }
    }
}