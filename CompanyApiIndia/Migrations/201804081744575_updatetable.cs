namespace CompanyApiIndia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IndianCompanies", "RegionIdentifier", c => c.String());
            AddColumn("dbo.IndianCompanies", "_corporate_identification_number_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_date_of_registration_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_name_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_status_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_class_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_category_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_authorized_capital_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_paidup_capital_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_registered_state_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_registrar_of_companies_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_principal_business_activity_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_registered_office_address_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_sub_category_", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.IndianCompanies", "_sub_category_");
            DropColumn("dbo.IndianCompanies", "_registered_office_address_");
            DropColumn("dbo.IndianCompanies", "_principal_business_activity_");
            DropColumn("dbo.IndianCompanies", "_registrar_of_companies_");
            DropColumn("dbo.IndianCompanies", "_registered_state_");
            DropColumn("dbo.IndianCompanies", "_paidup_capital_");
            DropColumn("dbo.IndianCompanies", "_authorized_capital_");
            DropColumn("dbo.IndianCompanies", "_company_category_");
            DropColumn("dbo.IndianCompanies", "_company_class_");
            DropColumn("dbo.IndianCompanies", "_company_status_");
            DropColumn("dbo.IndianCompanies", "_company_name_");
            DropColumn("dbo.IndianCompanies", "_date_of_registration_");
            DropColumn("dbo.IndianCompanies", "_corporate_identification_number_");
            DropColumn("dbo.IndianCompanies", "RegionIdentifier");
        }
    }
}
