namespace CompanyApiIndia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IndianCompanies", "Name2", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.IndianCompanies", "Name2");
        }
    }
}
