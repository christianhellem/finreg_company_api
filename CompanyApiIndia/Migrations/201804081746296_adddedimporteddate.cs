namespace CompanyApiIndia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adddedimporteddate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IndianCompanies", "Imported", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.IndianCompanies", "Imported");
        }
    }
}
