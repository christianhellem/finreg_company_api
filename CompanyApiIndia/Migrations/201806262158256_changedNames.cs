namespace CompanyApiIndia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedNames : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IndianCompanies", "corporate_identification_number", c => c.String());
            AddColumn("dbo.IndianCompanies", "date_of_registration", c => c.String());
            AddColumn("dbo.IndianCompanies", "company_name", c => c.String());
            AddColumn("dbo.IndianCompanies", "company_status", c => c.String());
            AddColumn("dbo.IndianCompanies", "company_class", c => c.String());
            AddColumn("dbo.IndianCompanies", "company_category", c => c.String());
            AddColumn("dbo.IndianCompanies", "authorized_capital", c => c.String());
            AddColumn("dbo.IndianCompanies", "paidup_capital", c => c.String());
            AddColumn("dbo.IndianCompanies", "registered_state", c => c.String());
            AddColumn("dbo.IndianCompanies", "registrar_of_companies", c => c.String());
            AddColumn("dbo.IndianCompanies", "principal_business_activity", c => c.String());
            AddColumn("dbo.IndianCompanies", "registered_office_address", c => c.String());
            AddColumn("dbo.IndianCompanies", "sub_category", c => c.String());
            DropColumn("dbo.IndianCompanies", "_corporate_identification_number_");
            DropColumn("dbo.IndianCompanies", "_date_of_registration_");
            DropColumn("dbo.IndianCompanies", "_company_name_");
            DropColumn("dbo.IndianCompanies", "_company_status_");
            DropColumn("dbo.IndianCompanies", "_company_class_");
            DropColumn("dbo.IndianCompanies", "_company_category_");
            DropColumn("dbo.IndianCompanies", "_authorized_capital_");
            DropColumn("dbo.IndianCompanies", "_paidup_capital_");
            DropColumn("dbo.IndianCompanies", "_registered_state_");
            DropColumn("dbo.IndianCompanies", "_registrar_of_companies_");
            DropColumn("dbo.IndianCompanies", "_principal_business_activity_");
            DropColumn("dbo.IndianCompanies", "_registered_office_address_");
            DropColumn("dbo.IndianCompanies", "_sub_category_");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IndianCompanies", "_sub_category_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_registered_office_address_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_principal_business_activity_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_registrar_of_companies_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_registered_state_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_paidup_capital_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_authorized_capital_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_category_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_class_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_status_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_company_name_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_date_of_registration_", c => c.String());
            AddColumn("dbo.IndianCompanies", "_corporate_identification_number_", c => c.String());
            DropColumn("dbo.IndianCompanies", "sub_category");
            DropColumn("dbo.IndianCompanies", "registered_office_address");
            DropColumn("dbo.IndianCompanies", "principal_business_activity");
            DropColumn("dbo.IndianCompanies", "registrar_of_companies");
            DropColumn("dbo.IndianCompanies", "registered_state");
            DropColumn("dbo.IndianCompanies", "paidup_capital");
            DropColumn("dbo.IndianCompanies", "authorized_capital");
            DropColumn("dbo.IndianCompanies", "company_category");
            DropColumn("dbo.IndianCompanies", "company_class");
            DropColumn("dbo.IndianCompanies", "company_status");
            DropColumn("dbo.IndianCompanies", "company_name");
            DropColumn("dbo.IndianCompanies", "date_of_registration");
            DropColumn("dbo.IndianCompanies", "corporate_identification_number");
        }
    }
}
