namespace CompanyApiIndia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createtableproperties : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.IndianCompanies", "Name");
            DropColumn("dbo.IndianCompanies", "Name2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.IndianCompanies", "Name2", c => c.String());
            AddColumn("dbo.IndianCompanies", "Name", c => c.String());
        }
    }
}
