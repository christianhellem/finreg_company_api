﻿using NorwegianShareHolders.Finders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NorwegianShareHolders
{
    public partial class DbModel
    {
        public class NorwegianShareHolder : INorwegianShareHolderEntity
        {
            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public Guid Key { get; set; }

            public DateTime Imported { get; set; }

            public string OrganizationNumber { get; set; }

            public string OrganizationName { get; set; }

            public string ShareClass { get; set; }

            public string ShareHolderName { get; set; }

            public string OwnerOrganization { get; set; }

            public string OwnerPersonBirthYear { get; set; }

            public string OwnerPartialAddress { get; set; }

            public long NumberOfShares { get; set; }

            public long NumberOfSharesCompany { get; set; }

            public Guid Id => Key;
        }
    }
}