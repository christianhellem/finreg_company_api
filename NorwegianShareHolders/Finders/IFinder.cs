﻿using System;
using System.Collections.Generic;

namespace NorwegianShareHolders.Finders
{
    public interface IFinder<T> where T : INorwegianShareHolderEntity
    {
        T Get(Guid Id);
        IEnumerable<T> GetAll();
        void ClearCache();
    }
}