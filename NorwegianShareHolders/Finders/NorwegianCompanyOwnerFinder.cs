﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static NorwegianShareHolders.DbModel;

namespace NorwegianShareHolders.Finders
{
    public class NorwegianCompanyOwnerFinder
    {
        protected IList<NorwegianCompanyOwner> entitiesCache;

        protected readonly DbContext context;

        public NorwegianCompanyOwnerFinder()
        {
            this.context = new NorwegianShareHolderContext();
        }

        public NorwegianCompanyOwner GetCompanyOwner(string companyNumber)
        {
            return Entities().Where(x => x.OrganizationNumber == companyNumber).FirstOrDefault();
        }

        internal IEnumerable<NorwegianCompanyOwner> Entities()
        {
            return (entitiesCache ?? (entitiesCache = context.Set<NorwegianCompanyOwner>().ToList())).AsQueryable();
        }
    }
}
