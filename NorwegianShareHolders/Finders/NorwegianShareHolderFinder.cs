﻿using System.Collections.Generic;
using System.Linq;
using static NorwegianShareHolders.DbModel;

namespace NorwegianShareHolders.Finders
{
    public class NorwegianShareHolderFinder : Finder<NorwegianShareHolder>
    {
        public NorwegianShareHolderFinder(NorwegianShareHolderContext context) : base(context) { }

        internal override IEnumerable<NorwegianShareHolder> Entities()
        {
            return (entitiesCache ?? (entitiesCache = context.Set<NorwegianShareHolder>().ToList())).AsQueryable();
        }
    }
}
