﻿using NorwegianShareHolders.Finders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NorwegianShareHolders
{
    public class NorwegianCompanyOwner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(9)]
        public string OrganizationNumber { get; set; }

        public bool HasCompanyOwner { get; set; }
    }
}
