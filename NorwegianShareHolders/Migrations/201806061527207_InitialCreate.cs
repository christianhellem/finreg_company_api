namespace NorwegianShareHolders.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NorwegianShareHolders",
                c => new
                    {
                        Key = c.Guid(nullable: false, identity: true),
                        Imported = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Key);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NorwegianShareHolders");
        }
    }
}
