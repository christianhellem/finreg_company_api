namespace NorwegianShareHolders.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NorwegianShareHolders", "OrganizationNumber", c => c.String());
            AddColumn("dbo.NorwegianShareHolders", "OrganizationName", c => c.String());
            AddColumn("dbo.NorwegianShareHolders", "ShareClass", c => c.String());
            AddColumn("dbo.NorwegianShareHolders", "ShareHolderName", c => c.String());
            AddColumn("dbo.NorwegianShareHolders", "OwnerOrganization", c => c.String());
            AddColumn("dbo.NorwegianShareHolders", "OwnerPersonBirthYear", c => c.String());
            AddColumn("dbo.NorwegianShareHolders", "OwnerPartialAddress", c => c.String());
            AddColumn("dbo.NorwegianShareHolders", "NumberOfShares", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NorwegianShareHolders", "NumberOfShares");
            DropColumn("dbo.NorwegianShareHolders", "OwnerPartialAddress");
            DropColumn("dbo.NorwegianShareHolders", "OwnerPersonBirthYear");
            DropColumn("dbo.NorwegianShareHolders", "OwnerOrganization");
            DropColumn("dbo.NorwegianShareHolders", "ShareHolderName");
            DropColumn("dbo.NorwegianShareHolders", "ShareClass");
            DropColumn("dbo.NorwegianShareHolders", "OrganizationName");
            DropColumn("dbo.NorwegianShareHolders", "OrganizationNumber");
        }
    }
}
