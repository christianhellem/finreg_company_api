namespace NorwegianShareHolders.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<NorwegianShareHolders.DbModel.NorwegianShareHolderContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "NorwegianShareHolders.DbModel+NorwegianShareHolderContext";
        }

        protected override void Seed(NorwegianShareHolders.DbModel.NorwegianShareHolderContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
