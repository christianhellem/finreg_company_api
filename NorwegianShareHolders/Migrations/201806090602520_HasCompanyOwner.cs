namespace NorwegianShareHolders.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HasCompanyOwner : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NorwegianCompanyOwners",
                c => new
                    {
                        OrganizationNumber = c.String(nullable: false, maxLength: 9, unicode: false),
                        HasCompanyOwner = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrganizationNumber);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NorwegianCompanyOwners");
        }
    }
}
