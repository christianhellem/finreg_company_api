namespace NorwegianShareHolders.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToLong : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NorwegianShareHolders", "NumberOfSharesCompany", c => c.Long(nullable: false));
            AlterColumn("dbo.NorwegianShareHolders", "NumberOfShares", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.NorwegianShareHolders", "NumberOfShares", c => c.Int(nullable: false));
            DropColumn("dbo.NorwegianShareHolders", "NumberOfSharesCompany");
        }
    }
}
