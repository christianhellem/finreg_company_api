﻿using System.Data.Entity;
using System.Linq;

namespace NorwegianShareHolders
{
    public partial class DbModel
    {
        public class NorwegianShareHolderContext : DbContext
        {
            public NorwegianShareHolderContext() : base("name=NorwegianShareHolders")
            {
            }

            public DbSet<NorwegianShareHolder> ShareHolders { get; set; }
            public DbSet<NorwegianCompanyOwner> CompanyOwner { get; set; }
        }
    }
}