﻿using CompanyApiIndia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CompanyApiIndia.DbModel;

namespace RunImportIndia
{
    class RunImportIndia
    {
        static void Main(string[] args)
        {
            IndianApiConnection indianApi = new IndianApiConnection();

            indianApi.ClearCompanies();
            indianApi.GetData();
            
            Console.WriteLine("Done...");
            //Console.ReadLine();
        }
    }
}