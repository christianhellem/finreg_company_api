﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinregCompanyApi;

namespace ConsoleTestApi
{
    class ConsoleTestApi
    {
        static void Main(string[] args)
        {
            Console.Write("Enter Company number: ");
            //var cnr = Console.ReadLine();
            var cnr = "890610552";
            using (var API = new CompanyAPI(CountryCode.NO))
            {
                var company = new Company();
                //company = API.GetCompanyByNumber("02248765");
                if (API.GetCompanyByNumber(cnr, out company))
                    Console.WriteLine(company.JSON);
                else
                    Console.WriteLine("Company Not Found");
            }
            Console.ReadKey();
        }
    }
}