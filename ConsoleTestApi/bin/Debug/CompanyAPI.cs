﻿//-----------------------------------------------------------------------
// <copyright file="CompanyAPI.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using FinregCompanyApi.Integrations.AU;
using FinregCompanyApi.Integrations.IN;
using FinregCompanyApi.Integrations.NO;
using FinregCompanyApi.Integrations.UK;
using System;

namespace FinregCompanyApi
{
	public class CompanyAPI : IDisposable, ICompanyApi
	{
		ICompanyApi Api;

		//<!--<add key = "UkCompanyHouseIdentification" value="AkwEzLv4HApIrK0NsWlYJ6TQv9wFvCXq8IyJEmHD" />
		//  <add key = "UkCompanyHouseApiURI" value="https://api.companieshouse.gov.uk" />-->

		public CompanyAPI(CountryCode countryCode, string URI = "", string Identification = "")
		{
			switch (countryCode)
			{
				case CountryCode.UK:
					Api = new UkApi(Identification,URI);
					break;
				case CountryCode.IN:
					Api = new InApi();
					break;
				case CountryCode.NO:
					Api = new NorwegianApi(URI);
					break;
				case CountryCode.AU:
					Api = new AustralianApi(Identification, URI);
					break;
				default:
					break;
			}
		}

		public Company GetCompanyByNumber(string companyNumber)
		{
			return Api.GetCompanyByNumber(companyNumber);
		}

		public bool GetCompanyByNumber(string companyNumber, out Company company)
		{
			return Api.GetCompanyByNumber(companyNumber, out company);
		}

		public void Dispose()
		{
			//implement
		}
	}
}