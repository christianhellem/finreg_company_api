using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using static AustralianCompanies.DbModel;

namespace RunImportAustralia
{
	class Program
	{
		private static string australianFolder;


		private static string fileSuffix { get { return @"csv"; } }

		static void Main(string[] args)
		{
			australianFolder = ConfigurationManager.AppSettings["AustralianCompaniesFolder"];

			Console.WriteLine("## RunImportAustralia ##");
			Console.WriteLine("------------------------");
			Console.WriteLine("");

			ImportFromCsv();

			Console.ReadKey();
		}

		private static void ImportFromCsv()
		{
			Console.WriteLine(string.Format("Importing from folder {0}", australianFolder));

			var directory = new DirectoryInfo(australianFolder);
			FileInfo[] Files = directory.GetFiles(string.Format("*.{0}", fileSuffix));

			if (Files.Length == 0)
			{
				Console.WriteLine("No files found");
				return;
			}

			var nmbr = 0;

			try
			{
				using (var asd = new AustralianCompaniesContext())
				{
					asd.Database.ExecuteSqlCommand("TRUNCATE TABLE [AustralianCompanies]");
				}
			}
			catch (Exception)
			{
			}
			
			foreach (var file in Files)
			{
				Console.WriteLine(string.Format("Reading file {0}", file.Name));

				var lines = File.ReadAllLines(file.FullName);
				var companyEntities = new List<AustralianCompany>();

				nmbr = lines.Length;
				//nmbr = 100000;

				var firstline = lines[1].Split("\t".ToCharArray());
				AustralianCompaniesContext context = null;
				context = new AustralianCompaniesContext();
				context.Configuration.AutoDetectChangesEnabled = false;
				context = AddToContext(context, new AustralianCompany(firstline), 1, 1, true);


				for (int i = 2; i < nmbr; i++)
				{
					decimal percentage = ((decimal)i / (decimal)nmbr) * 100M;

					Console.Write(string.Format("\rReading companies {0}/{1}, {2}{3}", i, nmbr, ((int)percentage).ToString(), "% ..."));
					string[] line = lines[i].Split("\t".ToCharArray());
					//companyEntities.Add(new AustralianCompany(line));

					var insertcmn = string.Format("INSERT INTO[dbo].[AustralianCompanies] "
							+ "([CompanyName]				   "
							+ "  ,[ACN]						   "
							+ "  ,[Type]					   "
							+ "  ,[Class]					   "
							+ "  ,[SubClass]				   "
							+ "  ,[Status]					   "
							+ "  ,[DateofRegistration]		   "
							+ "  ,[PreviousStateofRegistration] "
							+ "  ,[StateRegistrationnumber]	  "
							+ "  ,[Modifiedsincelastreport]	  "
							+ "  ,[CurrentNameIndicator]	  "
							+ "  ,[ABN]						  "
							+ "  ,[CurrentName]				  "
							+ "  ,[CurrentNameStartDate])	  "
							+ "VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}')",
							line[0].Replace("'","''"),
							line[1],
							line[2],
							line[3],
							line[4],
							line[5],
							line[6],
							line[7],
							line[8],
							line[9],
							line[10],
							line[11],
							line[12].Replace("'", "''"),
							line[13]);

					context.Database.ExecuteSqlCommand(insertcmn);
				}

				Console.WriteLine(string.Format("\rReading companies {0}/{1}, {2}{3}", nmbr, nmbr, 100, "% ... done!"));

				//try
				//{
				//	using (var asd = new AustralianCompaniesContext())
				//	{
				//		asd.Database.ExecuteSqlCommand("TRUNCATE TABLE [AustralianCompanies]");
				//	}
				//}
				//catch (Exception)
				//{
				//}

				//AustralianCompaniesContext context = null;
				//try
				//{
				//	context = new AustralianCompaniesContext();
				//	context.Configuration.AutoDetectChangesEnabled = false;

				//	int count = 0;
				//	for (int i = 0; i < companyEntities.Count; i++)
				//	{
				//		++count;

				//		decimal percentage = ((decimal)i / (decimal)nmbr) * 100M;

				//		Console.Write(string.Format("\rPopulating database {0}/{1}, {2}{3}", i, nmbr, ((int)percentage).ToString(), "% ..."));

				//		context = AddToContext(context, companyEntities[i], count, 500, true);


				//	}

				//	Console.WriteLine(string.Format("\rPopulating database {0}/{1}, {2}{3}", nmbr, nmbr, 100, "% ... done!"));

				//	foreach (var entityToInsert in companyEntities)
				//	{
				//		++count;
				//		context = AddToContext(context, entityToInsert, count, 100, true);
				//	}

				//	context.SaveChanges();
				//}
				//finally
				//{
				//	if (context != null)
				//		context.Dispose();
				//}

				Console.WriteLine("Import done!");

				Console.ReadKey();
			}
		}

		private static AustralianCompaniesContext AddToContext(AustralianCompaniesContext context, AustralianCompany entity, int count, int commitCount, bool recreateContext)
		{
			context.Set<AustralianCompany>().Add(entity);

			if (count % commitCount == 0)
			{
				context.SaveChanges();
				if (recreateContext)
				{
					context.Dispose();
					context = new AustralianCompaniesContext();
					context.Configuration.AutoDetectChangesEnabled = false;
				}
			}
			return context;
		}
	}
}
