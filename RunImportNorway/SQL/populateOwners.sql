insert into [dbo].[NorwegianCompanyOwners]
select Orgs.OrgNbr as OrganizationNumber,case when (isnull(owned.Ownr,-1) = -1) then 0 else 1 end as HasCompanyOwner from 
(
	select distinct(OrganizationNumber) OrgNbr from [dbo].[NorwegianShareHolders]
) Orgs left join 
(
select max([OwnerOrganization]) Ownr,OrganizationNumber  from [dbo].[NorwegianShareHolders]
group by OrganizationNumber
) owned on owned.OrganizationNumber = Orgs.OrgNbr
