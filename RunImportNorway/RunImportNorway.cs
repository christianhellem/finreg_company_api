﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using static NorwegianShareHolders.DbModel;

namespace RunImportNorway
{
    class RunImportNorway
    {
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");

            Console.WriteLine("Running import of norwegian shareholders");

            ExcelImport();
                    
            Console.ReadKey();
        }


        private static NorwegianShareHolderContext AddToContext(NorwegianShareHolderContext context, NorwegianShareHolder entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<NorwegianShareHolder>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new NorwegianShareHolderContext();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        static private void ExcelImport()
        {

            Console.Write("Reading excel file...");

            var originalFileName = "NorwegianShareHolders.xlsx";
            var file = new FileInfo(originalFileName);
            DataTableCollection dataTables;

            using (var stream = File.Open(originalFileName, FileMode.Open, FileAccess.Read))
            {
                IExcelDataReader reader;
                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                    }
                };
                var dataSet = reader.AsDataSet();
                dataTables = dataSet.Tables;
            }

            Console.WriteLine("done!");
            Console.Write("Parsing spreadsheet data...");

            var shareholders = new List<NorwegianShareHolder>();

            foreach (DataTable table in dataTables)
            {
                for (int i = 1; i < table.Rows.Count; i++)
                {
                    shareholders.Add(new NorwegianShareHolder()
                    {
                        Imported = DateTime.Now,
                        NumberOfShares = long.Parse(table.Rows[i][6].ToString()),
                        NumberOfSharesCompany = long.Parse(table.Rows[i][7].ToString()),
                        OrganizationNumber = table.Rows[i][0].ToString(),
                        OrganizationName = table.Rows[i][1].ToString(),
                        ShareClass = table.Rows[i][2].ToString(),
                        ShareHolderName = table.Rows[i][3].ToString(),
                        OwnerPersonBirthYear = (table.Rows[i][4].ToString().Length == 4) ? table.Rows[i][4].ToString() : null,
                        OwnerOrganization = (table.Rows[i][4].ToString().Length == 9) ? table.Rows[i][4].ToString() : null,
                        OwnerPartialAddress = table.Rows[i][5].ToString()
                    });
                }
            }

            using (var asd = new NorwegianShareHolderContext())
            {
                asd.Database.ExecuteSqlCommand("TRUNCATE TABLE [NorwegianShareHolders]");
            }
            Console.WriteLine("done!");
            Console.Write("Populating database...");

            NorwegianShareHolderContext context = null;
            try
            {
                context = new NorwegianShareHolderContext();
                context.Configuration.AutoDetectChangesEnabled = false;

                int count = 0;
                foreach (var entityToInsert in shareholders)
                {
                    ++count;
                    context = AddToContext(context, entityToInsert, count, 100, true);
                }

                context.SaveChanges();
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }

            Console.WriteLine("done!");
            Console.ReadKey();
        }
    }
}
