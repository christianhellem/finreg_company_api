﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FinRegApiWebTest.Startup))]
namespace FinRegApiWebTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
