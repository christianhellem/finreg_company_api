﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinregCompanyApi;

namespace FinRegApiWebTest.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "";
			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "";
			return View();
		}

		[HttpPost]
		public ActionResult GetCompany(string param)
		{
			//using (var API =
			//			new CompanyAPI(
			//				CountryCode.UK,
			//				@"https://api.companieshouse.gov.uk",
			//				"AkwEzLv4HApIrK0NsWlYJ6TQv9wFvCXq8IyJEmHD"))


			using (var API =
						new CompanyAPI(
							CountryCode.NO,
							@"https://data.brreg.no/enhetsregisteret/api"))

			//using (var API =
			//		new CompanyAPI(
			//			CountryCode.AU,
			//			@"https://abr.business.gov.au/abrxmlsearch/AbrXmlSearch.asmx/SearchByABNv201408?searchString={0}&includeHistoricalDetails=N&authenticationGuid={1}",
			//			"1efb473d-e381-4b3a-a569-98c8f4eb727f"))

			{
				var jsonresult = Json(API.GetCompanyByNumber(param).JSON, JsonRequestBehavior.AllowGet);
				return jsonresult;
			}
		}
	}
}