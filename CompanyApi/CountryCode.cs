﻿//-----------------------------------------------------------------------
// <copyright file="CountryCode.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FinregCompanyApi
{
	public enum CountryCode
	{
		UK,
		IN,
		NO,
		AU
	}
}