﻿//-----------------------------------------------------------------------
// <copyright file="ICompanyApi.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FinregCompanyApi
{
	public interface ICompanyApi
	{
		Company GetCompanyByNumber(string companyNumber);
		bool GetCompanyByNumber(string companyNumber, out Company company);
		void Dispose();
	}
}