﻿//-----------------------------------------------------------------------
// <copyright file="Company.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace FinregCompanyApi
{
	public class Company
	{
		private string _JSON;

		public string JSON
		{
			get { return _JSON; }

			set { _JSON = value; }
		}
	}
}