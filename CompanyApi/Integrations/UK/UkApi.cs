﻿//-----------------------------------------------------------------------
// <copyright file="UkApi.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace FinregCompanyApi.Integrations.UK
{

	public class UkApi : IDisposable, ICompanyApi
	{
		//pw company reg api Password@123
		HttpWebRequest request;

		public UkApi(string identification, string uRI)
		{
			//this.identification = identification;

			this.UkApiUser = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(identification + ":"));
			this.UKApiUri = uRI;
		}

		private string UKApiUri { get; }

		private string UkApiUser { get; }


		public bool GetCompanyByNumber(string companyNumber, out Company company)
		{
			company = new Company();
			company.JSON = this.GetCompanyFromUKAPI(companyNumber);
			return company.JSON.Length > 0;
		}

		public Company GetCompanyByNumber(string companyNumber)
		{
			var company = new Company();
			company.JSON = GetCompanyFromUKAPI(companyNumber);
			return company;
		}

		private string GetCompanyFromUKAPI(string companyNumber)
		{
			var apiMethod = "/company/" + companyNumber;

			var companyResult = CallApi(apiMethod);

			try
			{
				var jsonCompany = JObject.Parse(companyResult);

				var sigJson = (string)jsonCompany["links"]["persons_with_significant_control"];
				if (!string.IsNullOrWhiteSpace(sigJson))
				{
					JObject jsonSigPersons = JObject.Parse(CallApi(sigJson));

					//var test = persons-with-significant-control
					var individuals = jsonSigPersons.SelectToken("items").Where(x => x["kind"].ToString() == "individual-person-with-significant-control");
					var companies = jsonSigPersons.SelectToken("items").Where(x => x["kind"].ToString() == "corporate-entity-person-with-significant-control");

					jsonCompany.Add("is_companies_in_control", (companies.Count() > 0) ? true : false);

					string i = "[";
					foreach (var individual in individuals)
						i += individual.ToString() + ",";

					if (individuals.Count() > 0)
					{
						i.Remove(i.Length - 1);
						i += "]";
						jsonCompany.Add("persons_with_control", JToken.Parse(i));
					}
					string c = "[";
					foreach (var company in companies)
						c += company.ToString() + ",";

					if (companies.Count() > 0)
					{
						c.Remove(c.Length - 1);
						c += "]";
						jsonCompany.Add("companies_with_control", JToken.Parse(c));
					}
				}
				companyResult = Regex.Replace(jsonCompany.ToString(), @"\t|\n|\r", string.Empty);
			}
			catch
			{//fall through -- something wrong with the significant persons  
			}

			return companyResult;
		}

		private string CallApi(string apiMethod)
		{
			try
			{
				string result = string.Empty;
				//ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

				request = (HttpWebRequest)WebRequest.Create(this.UKApiUri + apiMethod);
				request.Headers.Add("Authorization", "Basic " + this.UkApiUser);

				using (var response = (HttpWebResponse)request.GetResponse())
				{
					using (var reader = new StreamReader(response.GetResponseStream()))
					{
						result = reader.ReadToEnd();
					}
				}
				return result;
			}
			catch (WebException)
			{
				//response from server but company not found
				return string.Empty;
			}
		}

		public void Dispose()
		{
			//Implemnent disposal
		}
	}
}