﻿//-----------------------------------------------------------------------
// <copyright file="NorwegianApi.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using Newtonsoft.Json.Linq;
using NorwegianShareHolders.Finders;
using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;

namespace FinregCompanyApi.Integrations.NO
{
	public class NorwegianApi : ICompanyApi, IDisposable
	{
		HttpWebRequest request;

		public NorwegianApi(string uRI)
		{
			NorwegianApiURI = uRI;
		}

		private string NorwegianApiURI { get; }

		public Company GetCompanyByNumber(string companyNumber)
		{
			var company = new Company();
			company.JSON = GetCompanyFromNorwegianAPI(companyNumber);
			return company;
		}

		public bool GetCompanyByNumber(string companyNumber, out Company company)
		{
			company = new Company();
			company.JSON = this.GetCompanyFromNorwegianAPI(companyNumber);
			return company.JSON.Length > 0;
		}

		private bool? IsCompanyInControl(string companyNumber)
		{
			var finder = new NorwegianCompanyOwnerFinder();

			var companyOwner = finder.GetCompanyOwner(companyNumber);

			return companyOwner?.HasCompanyOwner;
		}



		private string GetCompanyFromNorwegianAPI(string companyNumber)
		{
			string mainCompanyResult = null;
			string subCompanyResult = null;
			string subCompaniesForSearchedCompany = null;
			bool isMainOrganization;

			try
			{
				mainCompanyResult = CallApi("/enheter/" + companyNumber);
			}
			catch (Exception)
			{
				isMainOrganization = false;
			}

			isMainOrganization = !string.IsNullOrEmpty(mainCompanyResult);

			if (!isMainOrganization)
			{
				isMainOrganization = false;
				try
				{
					subCompanyResult = CallApi("/underenheter/" + companyNumber);
				}
				catch (Exception)
				{
					throw new ValidationException("Company with number: " + companyNumber + " not found!");
				}
			}

			var jsonCompany = JObject.Parse(mainCompanyResult ?? subCompanyResult);
			jsonCompany.Add("Is_Sub_Company", !isMainOrganization);

			if (isMainOrganization)
			{
				//Company accountant?
				jsonCompany.Add("Company_Management_Or_Owner", GetCompanyLeader(companyNumber));

				jsonCompany.Add("Is_Companies_In_Control", IsCompanyInControl(companyNumber));

			}
			else
			{
				jsonCompany.Add("Parent_Company", JObject.Parse(GetCompanyFromNorwegianAPI(jsonCompany["overordnetEnhet"].ToString())));
			}

			try
			{
				subCompaniesForSearchedCompany = CallApi("/underenheter?overordnetEnhet=" + companyNumber);
				var jsonSubs = JObject.Parse(subCompaniesForSearchedCompany);
				var numberOfSubs = jsonSubs["page"]["totalElements"].Value<int>();
				if (numberOfSubs > 0)
				{
					jsonSubs.Remove("page");
					jsonSubs.AddFirst(new JProperty("Number_Of_Sub_Companies", numberOfSubs));
					var subs = jsonSubs["_embedded"]["underenheter"];
					jsonSubs.Remove("_embedded");
					jsonSubs.Add("Companies", subs);
					jsonCompany.Add("Sub_Companies", jsonSubs);
				}
			}
			catch (Exception e)
			{
			}
			return jsonCompany.ToString();
		}
		private string GetCompanyLeader(string companyNumber)
		{
			string result = "not found";

			try
			{
				request = (HttpWebRequest)WebRequest.Create("https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=" + companyNumber);
				request.Method = "GET";

				using (var response = (HttpWebResponse)request.GetResponse())
				{
					using (var reader = new StreamReader(response.GetResponseStream()))
					{
						result = reader.ReadToEnd();
					}
				}

				try
				{
					result = result.Substring(result.IndexOf("Daglig leder")).Substring(result.Substring(result.IndexOf("Daglig leder")).IndexOf("<p>")).Split("/".ToCharArray())[0].Split(">".ToCharArray())[1].TrimEnd("<".ToCharArray());

				}
				catch (Exception)
				{
					result = "not found";
				}
			
				if (result.Length > 75)
					result = "not found";
			}
			catch
			{
				result = "not found";
			}
			return result;
		}

		private string CallApi(string apiMethod)
		{
			try
			{
				string result = string.Empty;

				request = (HttpWebRequest)WebRequest.Create(this.NorwegianApiURI + apiMethod);
				request.Method = "GET";

				using (var response = (HttpWebResponse)request.GetResponse())
				{
					using (var reader = new StreamReader(response.GetResponseStream()))
					{
						result = reader.ReadToEnd();
					}
				}
				return result;
			}
			catch (WebException e)
			{
				//response from server but company not found
				return null;
			}
		}

		public void Dispose()
		{
			//implement;
		}
	}
}
