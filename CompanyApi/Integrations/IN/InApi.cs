﻿//-----------------------------------------------------------------------
// <copyright file="InApi.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;

namespace FinregCompanyApi.Integrations.IN
{
	internal class InApi : ICompanyApi, IDisposable
	{
		public InApi()
		{
			
		}

		// Created as direct import to db.. for now
		//API KEY: 579b464db66ec23bdd000001df862bf7324f4e78636e51e0daf14292

		public Company GetCompanyByNumber(string companyNumber)
		{
			throw new NotImplementedException();
		}

		public bool GetCompanyByNumber(string companyNumber, out Company company)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			//todo
		}
	}
}