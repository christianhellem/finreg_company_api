﻿//-----------------------------------------------------------------------
// <copyright file="AustralianApi.cs">
//     Author: DAS Consulting
//	   Modified Date: 10-06-2019 
//     Copyright (c) DAS Consulting. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
//using AustralianCompanies.Finders;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;

namespace FinregCompanyApi.Integrations.AU
{
	public class AustralianApi : ICompanyApi
	{

		/*
		 * 58050345554
		 1efb473d-e381-4b3a-a569-98c8f4eb727f
		*/

		HttpWebRequest request;

		public AustralianApi(string identification, string uRI)
		{
			this.AustralianApiIdentification = identification;
			this.AustralianApiURI = uRI;
		}

		private string AustralianApiIdentification { get; }

		private string AustralianApiURI { get; }


		//From DB: 
		//public Company GetCompanyByNumber(string companyNumber)
		//{
		//	var finder = new AustralianCompanyFinder();

		//	var company = finder.GetAll().Where(x => x.ACN == companyNumber || x.ABN == companyNumber || x.StateRegistrationnumber == companyNumber).FirstOrDefault();

		//	if (company != null)
		//		return new Company() { JSON = new JavaScriptSerializer().Serialize(company) };

		//	return null;
		//}

		//From webservice
		public Company GetCompanyByNumber(string companyNumber)
		{
			try
			{
				string result = string.Empty;

				request = (HttpWebRequest)WebRequest.Create(string.Format(AustralianApiURI, companyNumber, AustralianApiIdentification));
				request.Method = "GET";

				using (var response = (HttpWebResponse)request.GetResponse())
				{
					using (var reader = new StreamReader(response.GetResponseStream()))
					{
						result = reader.ReadToEnd();
					}
				}
				return ResultToCompany(result);
			}
			catch (WebException)
			{
				//response from server but company not found
				return null;
			}
		}

		private Company ResultToCompany(string result)
		{

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(result);

			string json = JsonConvert.SerializeXmlNode(doc.GetElementsByTagName("response")[0]);

			return new Company() { JSON = json };

		}

		public bool GetCompanyByNumber(string companyNumber, out Company company)
		{
			company = GetCompanyByNumber(companyNumber);
			return company != null;
		}

		public void Dispose()
		{
		}
	}
}