﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinregCompanyApi;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace NetCoreTest.Pages
{
	public class IndexModel : PageModel
	{
		public void OnGet()
		{

		}

		public string SomeString()
		{
			using (var API =
						new CompanyAPI(
							CountryCode.UK,
							@"https://api.companieshouse.gov.uk",
							"AkwEzLv4HApIrK0NsWlYJ6TQv9wFvCXq8IyJEmHD"))
			{
				var comp = API.GetCompanyByNumber("00002065");

				return comp.JSON;

			}



		}
	}
}
