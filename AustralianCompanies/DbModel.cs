﻿using System.Data.Entity;
using System.Linq;

namespace AustralianCompanies
{
    public partial class DbModel
    {
        public class AustralianCompaniesContext : DbContext
        {
            public AustralianCompaniesContext() : base("name=AustralianCompanies")
            {
            }
			public DbSet<AustralianCompany> Companies { get; set; }
		}
	}
}