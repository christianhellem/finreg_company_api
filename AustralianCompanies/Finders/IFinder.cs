﻿using System;
using System.Collections.Generic;

namespace AustralianCompanies.Finders
{
    public interface IFinder<T> where T : IAustralianCompanyEntity
    {
        T Get(Guid Id);
        IEnumerable<T> GetAll();
        void ClearCache();
    }
}