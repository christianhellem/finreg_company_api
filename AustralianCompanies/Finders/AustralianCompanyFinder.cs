﻿using AustralianCompanies.Finders;
using System.Collections.Generic;
using System.Linq;
using static AustralianCompanies.DbModel;

namespace AustralianCompanies.Finders
{
    public class AustralianCompanyFinder : Finder<AustralianCompany>
    {
		public AustralianCompanyFinder() : base(new AustralianCompaniesContext())
		{	
		}

		public AustralianCompanyFinder(AustralianCompaniesContext context) : base(context) { }

        internal override IEnumerable<AustralianCompany> Entities()
        {
            return (entitiesCache ?? (entitiesCache = context.Set<AustralianCompany>().ToList())).AsQueryable();
        }
    }
}
