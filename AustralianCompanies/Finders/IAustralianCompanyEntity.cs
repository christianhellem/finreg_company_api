﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AustralianCompanies.Finders
{
    
    public interface IAustralianCompanyEntity
	{
        Guid Id { get; }
    }
}
