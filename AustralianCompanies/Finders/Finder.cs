﻿using AustralianCompanies.Finders;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AustralianCompanies.Finders
{
    public abstract class Finder<T> : IFinder<T> where T : IAustralianCompanyEntity
    {
        protected readonly DbContext context;
        protected IList<T> entitiesCache;
        private Type type;

        protected Finder(DbContext context)
        {
            this.context = context;
        }

        protected Finder(DbContext context, Type type)
        {
            this.context = context;
            this.type = type;
        }

        /// <summary>
        /// Get entity with a specific Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public T Get(Guid Id)
        {
            return Entities().Where(x => x.Id == Id).FirstOrDefault();
        }

        /// <summary>
        /// Get all entities of type T in the system
        /// </summary>
        /// <returns>Get All</returns>
        public IEnumerable<T> GetAll()
        {
            return Entities();
        }

        /// <summary>
        /// Clear the finder cache
        /// </summary>
        public void ClearCache()
        {
            entitiesCache = null;
        }

        internal abstract IEnumerable<T> Entities();
    }
}
