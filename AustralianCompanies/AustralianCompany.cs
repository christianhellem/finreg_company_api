﻿using AustralianCompanies.Finders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AustralianCompanies
{
    public partial class DbModel
    {
        public class AustralianCompany : IAustralianCompanyEntity
        {
			public AustralianCompany()
			{

			}

			public AustralianCompany(string[] line)
			{
				this.CompanyName = line[0];
				this.ACN = line[1];
				this.Type = line[2];
				this.Class = line[3];
				this.SubClass = line[4];
				this.Status = line[5];
				this.DateofRegistration = line[6];
				this.PreviousStateofRegistration = line[7];
				this.StateRegistrationnumber = line[8];
				this.Modifiedsincelastreport = line[9];
				this.CurrentNameIndicator = line[10];
				this.ABN = line[11];
				this.CurrentName = line[12];
				this.CurrentNameStartDate = line[13];
			}

			[Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public Guid Key { get; set; }
			public Guid Id => Key;
			public string CompanyName { get; set; }
			public string ACN { get; set; }
			public string Type { get; set; }
			public string Class { get; set; }
			public string SubClass { get; set; }
			public string Status { get; set; }
			public string DateofRegistration { get; set; }
			public string PreviousStateofRegistration { get; set; }
			public string StateRegistrationnumber { get; set; }
			public string Modifiedsincelastreport { get; set; }
			public string CurrentNameIndicator { get; set; }
			public string ABN { get; set; }
			public string CurrentName { get; set; }
			public string CurrentNameStartDate { get; set; }
        }
    }
}